




list.files(pattern = ".Rds", ignore.case = TRUE)
rds.filename <- "cross-validation_runtiming_sockeye_chilko_2023-01-05.Rds"
rds.filename <- "cross-validation_diversion_sockeye_fraser_2023-01-05.Rds"
data.list <- readRDS(rds.filename)
data.list$forecast.type
data.list$stock
head(data.list$res5)	

#top early stuart model 
formula.v <- "y.var ~ sst_0_2 + u_3_5 * v_3_5"
#top chilko model 
formula.v <- "y.var ~ sst_2_1 + u_3_1 + v_3_1 + cycle2020"
#top diversion model
formula.v <- "y.var ~ sst_1_5 + u_2_3 * v_2_3 + cycle2020"

lm.model <- function(x){
#browser()
	lmfit <- lm(formula.v, data = x$data)
	theta <- coefficients(lmfit)
	lmfit.summary <- summary(lmfit)
	theta.se <- lmfit.summary$coefficients[,2]
	forecast <- predict(lmfit, x$newdata)
list(year=x$newdata$year, theta=theta, theta.se=theta.se, forecast=forecast)
}

res <- performancer::retrospective(data.list$data.fitting.wide, testwindow = 20, FUN =  lm.model)

res <- lapply(res, function(x) data.frame(year=x$year, theta=t(x$theta),theta.se=t(x$theta.se), forecast=x$forecast))

res[[1]]

res <- do.call(rbind, res)
View(res)
data.list$retrospective <- res
saveRDS(data.list, rds.filename)

names(res)

varying <- colnames(res)[grepl("theta", colnames(res)) & !grepl("se", colnames(res))]
idvar <- "year"
colnames.include <- c(idvar, varying)
res.long.estimate <- reshape(res[,colnames.include], direction = "long", idvar = idvar, varying = list(varying), times = varying, timevar = "variable", v.names = "estimate")
res.long.estimate$variable <- gsub("theta|\\.*", "", res.long.estimate$variable)

head(res.long.estimate)

varying <- colnames(res)[grepl("theta", colnames(res)) & grepl("se", colnames(res))]
idvar <- "year"
colnames.include <- c(idvar, varying)
res.long.se <- reshape(res[,colnames.include], direction = "long", idvar = idvar, varying = list(varying), times = varying, timevar = "variable", v.names = "se")
res.long.se$variable <- gsub("theta|se|\\.*", "", res.long.se$variable)
head(res.long.se)
unique(res.long.se$variable)
unique(res.long.estimate$variable)

res.long <- merge(res.long.estimate, res.long.se, sort = FALSE)
res.long$lower <- res.long$estimate-res.long$se
res.long$upper <- res.long$estimate+res.long$se
head(res.long)

require(ggplot2)
dev.new()
ggplot(res.long, aes(year, estimate))+
	geom_line()+geom_point()+
	geom_errorbar(aes(ymin = lower, ymax = upper), width = 0.2)+
	scale_x_continuous(breaks = seq(2000,2025, by= 2))+
	facet_wrap(vars(variable), scales = "free_y", ncol = 1)
filename <- paste(data.list$forecast.type, "sockeye", data.list$stock, "retrospective_parameters.png", sep = "_")
filename
ggsave(filename, width = 7, height = 10, units = "in", dpi=400)
shell.exec(filename)


source("C:/Users/folkesm/Documents/Projects/salmon/FraserRiver/FraserSockeye/forecasts_timing/Timing2022/R/functions_graphics.R")

#ylab <- Median Timing Ordinal Day
filename <- paste(data.list$forecast.type, data.list$stock, "timeseries.png", sep="_")



ggplot(data=data.list$data.fitting.wide, aes(year, y.var))+
	geom_line()+geom_point()+
	scale_x_continuous(breaks = seq(1993,2025, by= 2))



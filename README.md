# runtiming R package



## Installation

To install `runtiming` you will need to have the R package `remotes` installed. 

In R:
```{r} 
install.packages("remotes") 
```

#`enviroData` is currently a private repository.

```{r} 
install.packages("git2r")
remotes::install_git("https://gitlab.com/michaelfolkes/runtiming")
```

Once the package is installed, if using Rstudio, you will find some basic information in the function help files.



## Demo scripts

The function `writeScript` will make it easier to start things. The help file has an example:
```
?runtiming::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "runtiming")
```

To save and open a specific demo script (this one named "demo.R"):
```
writeScript("demo")

```

Here is how to open a copy of the script that assesses timing and diversion forecast models:
```
writeScript("demo_forecast_timing_diversion.R")

```